from django.urls import path,reverse
from . import views

urlpatterns = [
    #path('simple_view/',views.simple_view), 
    #path('finance_view/',views.finance_view), 
    path('<str:topic>/',views.news_view,name='topic_views'), 
    path('<int:num1>/<int:num2>',views.add_view), 
    path('template_views',views.template_views)
]