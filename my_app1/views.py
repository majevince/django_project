from django.shortcuts import render
from django.http.response import  HttpResponse, HttpResponseNotFound,HttpResponseRedirect
from django.urls import reverse
# Create your function views here.
#def simple_view(request):
#    return HttpResponse("My Sample View web")



#def finance_view(request):
#    return HttpResponse("see if the  app are working fine")





articles = {
    'sports':'we all love sport',
    'finance':'fintech is the way forward',
    'politics':'for cash out'
}

def news_view(request,topic):
    try:
        result = articles[topic]
        return HttpResponse(result)
    except:
        result = "No Page found for this topic"
        return HttpResponseNotFound(result)

    

def add_view(request,num1,num2):
    add_result = num1 +num2
    result = f"{num1} + {num2} = {add_result}"
    return HttpResponse(str(result))



def num_page_view(reverse,num_page):
    topic_list = list(articles.keys())
    topics = topic_list[num_page]
    return HttpResponseRedirect(reverse('topic_views',args=[topics]))


def template_views(request):
    return render(request,'my_app1/example.html')