from django.urls import path
from .import views

app_name = 'my_app2'

urlpatterns = [
    path('',views.test_app,name='test_app'),
    path('variable_view',views.variable_view,name='variable_view')
]
