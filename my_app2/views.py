from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def test_app(request):
    return render(request,'my_app2/example.html')


def variable_view(request):
    my_var = {'first_name':'maje','last_name':'anyah',
              'some_list':[1,2,3,4],'user_login':True,
              
}
    return render(request,'my_app2/variable.html',context=my_var)