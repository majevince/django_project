from django.db import models
from django.core.validators import  MinValueValidator,MaxValueValidator,URLValidator

# Create your models here.
class patient(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    date_of_birth = models.DateTimeField()
    age = models.IntegerField()
    date_of_admission = models.DateTimeField(validators=[MinValueValidator(18),MaxValueValidator(90)])
    email_address = models.EmailField()
    heart_rate = models.IntegerField(default=60,validators=[MinValueValidator(0),MaxValueValidator(300)])

    def __str__(self):

        return f"{self.first_name},{self.last_name} is {self.age} years old, with email ID{self.email_address}"

