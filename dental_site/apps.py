from django.apps import AppConfig


class DentalSiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dental_site'
