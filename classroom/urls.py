from django.urls import  path
from .views import (HomeView, ThankYou,
                    ContactForm,TeacherCreateView,
                    TeacherListView,TeacherDetailView,
                    UpdateView,TeacherDeleteView
)


#app_name ='classroom'

urlpatterns = [
    path('', HomeView.as_view(),name='home_view'),
    path('ThankYou/', ThankYou.as_view(),name='thank_you'),
    path('contact/', ContactForm.as_view(),name='contact_us'),
    path('create_teacher/', TeacherCreateView.as_view(),name='create_teacher'),
    path('teacher_list/', TeacherListView.as_view(),name='teacher_list'),
    path('teacher_detail/<int:pk>/', TeacherDetailView.as_view(),name='teacher_detail'),
    path('teacher_edit/<int:pk>/', UpdateView.as_view(),name='teacher_update'),
    path('teacher_delete/<int:pk>/', TeacherDeleteView.as_view(),name='delete_teacher'),
]



