from django import forms




class Contact(forms.Form):

    name = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)