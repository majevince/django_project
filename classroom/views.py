from django.shortcuts import render
from django.views.generic import (TemplateView, FormView,
                                  CreateView,ListView,
                                  DetailView,UpdateView,
                                  DeleteView,
)
from .forms import  Contact
from django.urls import reverse_lazy,reverse
from .models import Teacher 
from django.forms import ModelForm, Textarea



#def home_view(request):
#    return render(request,'class_room/home.html')

class HomeView(TemplateView):
    template_name = 'class_room/home.html'


class ThankYou(TemplateView):

    template_name = 'class_room/thank_you.html'


class TeacherCreateView(CreateView):
    model = Teacher
    #fields = "__all__"
    template_name = 'class_room/teacher_form.html'
    fields = ('first_name','last_name','subject' )
    success_url = reverse_lazy('thank_you')
    widgets = { 'subject': Textarea(attrs={'cols': 40, 'rows': 10}), }
    labels = {
            
        }



class ContactForm(FormView):
    form_class = Contact

    template_name = 'class_room/contact.html'

    #success_url = '/classroom/ThankYou/'
    success_url = reverse_lazy('thank_you')

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
        
        

class TeacherListView(ListView):
    template_name = 'class_room/teacher_list.html'
    model = Teacher
    context_object_name = "teacher_list"
    queryset = Teacher.objects.order_by('first_name')

class TeacherDetailView(DetailView):
    model = Teacher
    template_name = 'class_room/teacher_detail.html'


class UpdateView(UpdateView):
    model = Teacher
    template_name = 'class_room/teacher_update.html'
    fields = "__all__"
    success_url = reverse_lazy('teacher_update')




class TeacherDeleteView(DeleteView):
    model = Teacher
    success_url = reverse_lazy('teacher_list')
    template_name = 'class_room/teacher_delete.html'

