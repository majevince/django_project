from django.db import models

# Create your models here.
class Review(models.Model):
    first_name = models.CharField(max_length=200)
    last_name  = models.CharField(max_length=200)
    email      = models.EmailField()
    review     = models.CharField(max_length=2000)