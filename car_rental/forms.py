from django import  forms
from .models import Review
from django.forms import ModelForm, Textarea

#class ReviewForm(forms.Form):
  #  first_name = forms.CharField(label='Fisrt Name',max_length=200)
 #   last_name  = forms.CharField(label='Last Name',max_length=200)
 #   email      = forms.EmailField(label='Email')
 #   review     = forms.CharField(label='Please Write your review Here',
#                                 widget=forms.Textarea(attrs={'rows':'5','cols':'5'}))



class ReviewForm(ModelForm):
    class Meta: 
        model = Review
        fields = ('first_name','last_name','email','review' )
        widgets = { 'review': Textarea(attrs={'cols': 40, 'rows': 10}), }
        labels = {
            
        }