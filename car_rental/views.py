from django.shortcuts import render, redirect
from django.urls import  reverse
from .forms import ReviewForm


#app_name = 'car_rental'

# Create your views here.
def rental_review(request):

    if request.method  == "POST":
        form = ReviewForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect(reverse('thank_you'))

    else:

        form = ReviewForm()


    return render(request,'car_rental/review.html', context={'form':form})
    
    

def thank_you(request):
    return render(request,'car_rental/thank_you.html')