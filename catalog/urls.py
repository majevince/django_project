from django.urls import path
from . import views



urlpatterns = [
    path('', views.Index,name='index'),
    path('create_book/', views.BookCreate.as_view(),name='create_book'),
    path('book/<int:pk>/', views.BookDetail.as_view(),name='detail_book'),
    path('my_view/', views.myView,name='my_view'),
    path('signup_view/', views.SignUpView.as_view(),name='signup_view'),
    path('profile/', views.CheckOutBookByUserView.as_view(),name='profile'),

]
