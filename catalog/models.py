from django.db import models
from django.urls import reverse
from django.contrib.auth.models import  User

# Create your models here.
class Genre(models.Model):
    
    
     name = models.CharField(max_length=150)

     def  __str__(self):
          return self.name
     

class Language(models.Model):
    name =models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Book(models.Model):
    
    
    title = models.CharField(max_length=200)
    author = models.ForeignKey('Author',on_delete=models.SET_NULL,null=True)
    summary = models.TextField(max_length=1000)
    isbn = models.CharField('ISBN',max_length=13,unique=True)
    language = models.ForeignKey('Language',on_delete=models.SET_NULL,null=True)
    genre = models.ManyToManyField(Genre)


    def  __str__(self):
        return self.title 
    
    def get_absolute_url(self):
        return reverse("book_detail", kwargs={"pk": self.pk})


class Author(models.Model):
    first_name =models.CharField(max_length=150)
    last_name =models.CharField(max_length=150)
    date_of_birth =models.DateTimeField(null=True,blank=True)
    genre = models.ManyToManyField(Genre)

    class Meta:
        ordering = ['last_name','first_name']
     
     

    def get_absolute_url(self):
        return reverse("author_detail", kwargs={"pk": self.pk})


    def __str__(self):
        return f"{self.last_name} , {self.first_name}"



import uuid

class Bookinstance(models.Model):

    id = models.UUIDField(primary_key=True,default=uuid.uuid4)
    book = models.ForeignKey('book',on_delete=models.RESTRICT,null=True)
    imprint = models.CharField(max_length=200)
    due_back = models.DateTimeField(null=True,blank=True)
    borrower = models.ForeignKey(User,on_delete=models.SET_NULL,null=True,blank=True)
 

    LOAN_STATUS = (
        ('m','Maintenance'),
        ('o','On Loan'),
        ('a','Available'),
        ('r','Reserved')
    )


    status = models.CharField(max_length=200,choices=LOAN_STATUS,default='m',blank=True)



    class Meta:

        ordering = ['due_back']

    def __str__(self):
        return f'{self.id} ({self.book.title})'